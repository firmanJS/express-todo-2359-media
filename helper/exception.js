'use strict';
const handler404 = (req, res) => {
  const err = process.env.NODE_ENV == 'development' ? new Error() : {};
  return res.status(404).json({
    'error': err.stack,
    'status': 404,
    'msg': `Route : ${req.url} Not found.`,
  });
};

const handler500 = (_req, res) => {
  const err = process.env.NODE_ENV == 'development' ? new Error() : {};
  res.status(500).json({
    'error': err.stack,
    'status': 500,
  });
};

const successResponse = (res, data, msg) => {
  return res.status(200).json({
    'status': `success`,
    'message': msg,
    'data': data,
  });
};

const notFoundResponse = (res) => {
  return res.status(404).json({
    'status': `empty`,
    'message': `Data not found`,
    'data': [],
  });
};

const errorResponse = (res, code, msg) => {
  require('dotenv').config();
  if (process.env.NODE_ENV == 'development') console.log(msg);
  return res.status(code).json({
    'status': `bad request`,
    'message': msg,
    'data': [],
  });
};


module.exports = {
  handler404, handler500, successResponse,
  notFoundResponse, errorResponse,
};
