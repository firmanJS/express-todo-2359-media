'use strict';
require('dotenv').config();
const jwt = require('jsonwebtoken');
const ls = require('local-storage');

const verifyToken = (req, res, next) => {
  if (ls.get('token') !== null) {
    jwt.verify(ls.get('token'), process.env.SECRET_KEY, (err) => {
      if (err) {
        return res.status(500).send({
          'status': 'Jwt invalid',
          'message': err,
        });
      }
      next();
    });
  } else {
    res.status(401).json({'message': 'Unauthorized'});
  }
};

module.exports = {
  verifyToken,
};
