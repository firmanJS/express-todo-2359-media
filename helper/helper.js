'user strict';
require('dotenv').config();
const jwt = require('jsonwebtoken');
const ls = require('local-storage');

const getJwt = () => {
  const token = ls.get('token');
  const decoded = jwt.verify(token, process.env.SECRET_KEY);
  return decoded.userToken;
};

module.exports = {
  getJwt,
};
