'use strict';
require('dotenv').config();
process.env.NODE_ENV = 'test';
const chai = require('chai');
const chaiHttp = require('chai-http');
const server = `http://localhost:${process.env.APP_PORT}${process.env.API_PATH}/`;
const assert = require('chai').assert;
const ls = require('local-storage');
chai.should();

chai.use(chaiHttp);

// Our parent block
describe('/POST Todo', () => {
  it('it should todo invalid no token', (done) => {
    const todos = {
      'taskname': 'ulang tahun boni',
      'time': '2020-05-18 10:00',
      'childtask': {'taskname': 'ulang tahun bona'},
      'latitude': '-6.917464',
      'longitude': '107.619125',
    };
    assert.typeOf(todos.taskname, 'string');
    assert.typeOf(todos.time, 'string');
    assert.typeOf(todos.latitude, 'string');
    assert.typeOf(todos.longitude, 'string');
    chai.request(server).post('todo')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(todos).then((res) => {
          res.status.should.be.equal(401);
          res.should.be.json;
          done();
        }).catch(function(err) {
          throw err;
        });
  });
});

describe('Todo', () => {
  it('it should login user valid user', (done) => {
    const users = {
      username: 'user',
      password: '123',
    };
    assert.typeOf(users.username, 'string');
    assert.typeOf(users.password, 'string');
    chai.request(server).post('user/login')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(users).then((res) => {
          ls.set('token', res.body.data.token);
          describe('/POST Todo', () => {
            it('it should todo valid with token', (dones) => {
              const todos = {
                'taskname': 'ulang tahun boni',
                'time': '2020-05-18 10:00',
                'childtask': {'taskname': 'ulang tahun bona'},
                'latitude': '-6.917464',
                'longitude': '107.619125',
              };
              assert.typeOf(todos.taskname, 'string');
              assert.typeOf(todos.time, 'string');
              assert.typeOf(todos.latitude, 'string');
              assert.typeOf(todos.longitude, 'string');
              chai.request(server).post('todo')
                  .set('Accept', 'application/json')
                  .set('Content-Type', 'application/json')
                  .send(todos).then((res) => {
                    res.status.should.be.equal(200);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    dones();
                  }).catch(function(err) {
                    throw err;
                  });
            });
          });
          done();
        }).catch(function(err) {
          throw err;
        });
  });
});

describe('Todo', () => {
  it('it should login user valid user', (done) => {
    const users = {
      username: 'user',
      password: '123',
    };
    assert.typeOf(users.username, 'string');
    assert.typeOf(users.password, 'string');
    chai.request(server).post('user/login')
        .set('Accept', 'application/json')
        .set('Content-Type', 'application/json')
        .send(users).then((res) => {
          ls.set('token', res.body.data.token);
          describe('/POST Todo', () => {
            it('it should todo invalid post with token', (dones) => {
              const todos = {
                'taskname': 'ulang tahun boni',
                'childtask': {'taskname': 'ulang tahun bona'},
                'latitude': '-6.917464',
                'longitude': '107.619125',
              };
              assert.typeOf(todos.taskname, 'string');
              assert.typeOf(todos.latitude, 'string');
              assert.typeOf(todos.longitude, 'string');
              chai.request(server).post('todo')
                  .set('Accept', 'application/json')
                  .set('Content-Type', 'application/json')
                  .send(todos).then((res) => {
                    res.status.should.be.equal(422);
                    res.should.be.json;
                    res.body.should.be.a('object');
                    dones();
                  }).catch(function(err) {
                    throw err;
                  });
            });
          });
          done();
        }).catch(function(err) {
          throw err;
        });
  });
});
