/* eslint-disable require-jsdoc */
'use strict';
const {todo} = require('../models/');
const msg = require('../helper/exception');
const moment = require('moment');
const {QueryTypes} = require('sequelize');

require('dotenv').config();
class todoController {
  static async create(res, payload) {
    try {
      const result = await todo.create(payload);
      msg.successResponse(res, result, 'Create todo successfull');
    } catch (error) {
      msg.errorResponse(res, 500, error);
    }
  }

  static async get(res, token) {
    try {
      const todays = moment().format('YYYY-MM-DD');
      let sql = `SELECT id,taskname,childtask::json`;
      sql +=`,to_char(time,'YYYY-MM-DD HH:MM') as time`;
      sql +=' FROM todos';
      sql += ` where userid = ${token.id} and time::date = '${todays}'`;
      const result = await todo.sequelize.query(sql, {type: QueryTypes.SELECT});
      msg.successResponse(res, result, 'Get data successfull');
    } catch (error) {
      msg.errorResponse(res, 500, error);
    }
  }

  static async update(req, res, payload, token) {
    let result;
    try {
      const todos = await todo.findOne({where: {
        id: req.params.id, userid: token.id,
      }});
      if (todos !== null) {
        result = 'Update data successfull';
        todos.update(payload);
      } else {
        result = 'Update data unsuccessfull id not found';
      }
      msg.successResponse(res, todos, result);
    } catch (error) {
      msg.errorResponse(res, 500, error);
    }
  }

  static async delete(req, res, token) {
    let result;
    try {
      const todos = await todo.findOne({where: {
        id: req.params.id, userid: token.id,
      }});
      if (todos !== null) {
        result = 'Delete data successfull';
        todos.destroy(todos);
      } else {
        result = 'Delete data unsuccessfull id not found';
      }
      msg.successResponse(res, todos, result);
    } catch (error) {
      msg.errorResponse(res, 500, error);
    }
  }
}

module.exports = todoController;
