/* eslint-disable require-jsdoc */
'use strict';
const passwordHash = require('password-hash');
const {user} = require('../models/');
const jwt = require('jsonwebtoken');
const msg = require('../helper/exception');
const ls = require('local-storage');

require('dotenv').config();

class userController {
  static async register(res, payload) {
    try {
      const result = await user.create(payload);
      msg.successResponse(res, result, 'Create user successfull');
    } catch (error) {
      msg.errorResponse(res, 500, error);
    }
  }

  static async login(param, res) {
    const check = await user.findOne({where: {username: param.username}});
    if (check) {
      const verify = passwordHash.verify(param.password, check.password);
      if (verify != true) {
        msg.errorResponse(res, 500, 'Password incorect');
      } else {
        const userToken = {
          id: check.id, username: check.username,
        };
        jwt.sign({userToken}, process.env.SECRET_KEY, {
          expiresIn: '3h', // set exipre token
        }, (_err, token) => {
          ls.set('token', token);
          msg.successResponse(res, {token: token}, 'Authenticate success');
        });
      }
    } else {
      msg.errorResponse(res, 500, 'Username or password not match');
    }
  }

  static async logout(res) {
    msg.successResponse(res, ls.remove('token'), 'Logout success');
  }
}

module.exports = userController;
