'use strict';
require('dotenv').config();

module.exports = {
  'development': {
    'username': process.env.DB_USERNAME_DEV,
    'password': process.env.DB_PASSWORD_DEV,
    'database': process.env.DB_DATABASE_DEV,
    'host': process.env.DB_HOST_DEV,
    'dialect': 'postgres',
    'port': 5432,
  },
  'stagging': {
    'username': process.env.DB_USERNAME_STAGGING,
    'password': process.env.DB_PASSWORD_STAGGING,
    'database': process.env.DB_DATABASE_STAGGING,
    'host': process.env.DB_HOST_STAGGING,
    'dialect': 'postgres',
    'logging': true,
    'port': 5432,
  },
  'production': {
    'username': process.env.DB_USERNAME_PRODUCTION,
    'password': process.env.DB_PASSWORD_PRODUCTION,
    'database': process.env.DB_DATABASE_PRODUCTION,
    'host': process.env.DB_HOST_PRODUCTION,
    'dialect': 'postgres',
    'port': 5432,
  },
};
