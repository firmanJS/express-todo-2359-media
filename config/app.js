'use strict';
const express = require('express');
const routing = require('./route');
const cors = require('cors');
const except = require('../helper/exception');
const compress = require('compression');
const methodOverride = require('method-override');
const helmet = require('helmet');
const sentry = require('@sentry/node');
const app = express();

require('dotenv').config();

sentry.init({dsn: process.env.SENTRY_APP}); // sentry config
app.use(sentry.Handlers.errorHandler()); // sentry config errorHandler reporting

// sentry config requestHandler reporting
app.use(sentry.Handlers.requestHandler());
app.use(express.json()); // body param
app.use(express.urlencoded({extended: true})); // if true enable multiple post
app.use(compress()); // gzip compression
app.use(methodOverride()); // lets you use HTTP verbs
app.use(helmet()); // secure apps by setting various HTTP headers
app.use(cors()); // enable cors
app.use(routing); // routing
app.use(except.handler404); // handler 404
app.use(except.handler500); // handler 500

module.exports = app;
