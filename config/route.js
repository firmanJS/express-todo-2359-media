'use strict';
const express = require('express');
const routing = express();
const index = require('../routes/index.route');
const users = require('../routes/user.route');
const todos = require('../routes/todo.route');

routing.use(index);
routing.use(users);
routing.use(todos);

module.exports = routing;
