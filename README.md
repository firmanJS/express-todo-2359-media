# Express todo app for 2359 media

## this app for the 2359 media

### How To run

#### 1. copy environment variable

```sh
cp .env-sample .env
```

#### 2. fill in the copied environment earlier

```sh
# development
DB_USERNAME_DEV=#username db
DB_PASSWORD_DEV=#password db
DB_DATABASE_DEV=#db name
DB_HOST_DEV=#db name

# stagging
DB_USERNAME_STAGGING=#username db
DB_PASSWORD_STAGGING=#password db
DB_DATABASE_STAGGING=#db name
DB_HOST_STAGGING=#db name

# production
DB_USERNAME_PRODUCTION=#username db
DB_PASSWORD_PRODUCTION=#password db
DB_DATABASE_PRODUCTION=#db name
DB_HOST_PRODUCTION=#db name

TZ=Asia/Jakarta
APP_PORT=3000
SECRET_KEY= #secret key token
NODE_ENV=development
API_PATH=/api
SENTRY_APP=#sentry app
```

#### 3. install package
```sh
npm install
```
#### 4. migration database
```sh
npx sequelize-cli db:migrate
```
#### 5. import postman collection
`2359 Media.postman_collection.json` to your postman app
#### 6. run app
```sh
npm run start
```
#### 7. run unit testing
```sh
npm run test
```

## enjoy 
