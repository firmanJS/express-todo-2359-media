'use strict';
const express = require('express');
const {check, validationResult} = require('express-validator');
const todos = require('../controllers/todo.controller');
const verify = require('../helper/auth');
const helper = require('../helper/helper');
const router = express.Router();

require('dotenv').config();

const apiUrl = process.env.API_PATH;
const schemaValidation = [
  check('taskname').not().isEmpty().isString(),
  check('time').not().isEmpty().isString(),
  check('latitude').not().isEmpty().isString(),
  check('longitude').not().isEmpty().isString(),
];

router
    .post(`${apiUrl}/todo`, verify.verifyToken, [schemaValidation], (req, res) => {
      const errors = validationResult(req);
      const token = helper.getJwt();
      const input = req.body;
      input['userid'] = token.id;
      if (!errors.isEmpty()) res.status(422).json(errors);
      todos.create(res, input);
    })

    .get(`${apiUrl}/todo`, verify.verifyToken, (req, res) => {
      todos.get(res, helper.getJwt());
    })

    .put(`${apiUrl}/todo/:id`, verify.verifyToken, [schemaValidation], (req, res) => {
      const errors = validationResult(req);
      const token = helper.getJwt();
      const input = req.body;
      input['userid'] = token.id;
      if (!errors.isEmpty()) res.status(422).json(errors);
      todos.update(req, res, input, helper.getJwt());
    })

    .delete(`${apiUrl}/todo/:id`, verify.verifyToken, (req, res) => {
      todos.delete(req, res, helper.getJwt());
    });

module.exports = router;
