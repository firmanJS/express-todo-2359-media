'use strict';
const express = require('express');
const {check, validationResult} = require('express-validator');
const users = require('../controllers/user.controller');
const passwordHash = require('password-hash');
const router = express.Router();

require('dotenv').config();

const apiUrl = process.env.API_PATH;
const registerValidation = [
  check('fullname').not().isEmpty().withMessage('required value'),
  check('username').not().isEmpty()
      .withMessage('required value').isAlphanumeric(),
  check('email').not().isEmpty().withMessage('required value').isEmail(),
  check('password').not().isEmpty()
      .withMessage('required value').isAlphanumeric(),
];

const loginValidation = [
  check('username').not().isEmpty().withMessage('required value'),
  check('password').not().isEmpty().withMessage('required value'),
];

const postSchema = (req) => {
  const input = req.body;
  input.password = passwordHash.generate(input.password);
  return input;
};

router
    .post(`${apiUrl}/user/register`, [registerValidation], (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) res.status(422).json(errors);
      users.register(res, postSchema(req));
    })

    .post(`${apiUrl}/user/login`, [loginValidation], (req, res) => {
      const errors = validationResult(req);
      if (!errors.isEmpty()) res.status(422).json(errors);
      users.login(req.body, res);
    })

    .post(`${apiUrl}/user/logout`, (req, res) => {
      users.logout(res);
    });

module.exports = router;
