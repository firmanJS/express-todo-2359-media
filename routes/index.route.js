'use strict';
const express = require('express');
const router = express.Router();

require('dotenv').config();

const format = (seconds) => {
  const pad = (s) => {
    return (s < 10 ? '0' : '') + s;
  };
  const hours = Math.floor(seconds / (60*60));
  const minutes = Math.floor(seconds % (60*60) / 60);
  const second = Math.floor(seconds % 60);

  return pad(hours) + ':' + pad(minutes) + ':' + pad(second);
};


router.get('/', (req, res) => {
  res.status(200).json({
    message: `welcome to express todo app for 2359 media`,
    data: {
      'status server': `http://${req.get('host')}/api/status`,
      'documentation postman': `http://${req.get('host')}/api/documentation`,
    },
  });
});

router.get(`/api/status`, (_req, res) => {
  const messages = {
    'info': `Server UP in ${format(require('os').uptime())}`,
  };
  res.status(200).json(messages);
});

router.get(`/api/documentation`, (_req, res) => {
  const readJson = require('../2359 Media.postman_collection.json');
  res.status(200).json(readJson);
});

module.exports = router;
