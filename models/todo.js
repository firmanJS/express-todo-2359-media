'use strict';
const moment = require('moment');
module.exports = (sequelize, DataTypes) => {
  const todo = sequelize.define('todo', {
    childtask: {
      type: DataTypes.STRING,
      get: function() {
        return JSON.parse(this.getDataValue('childtask'));
      },
      set: function(val) {
        return this.setDataValue('childtask', JSON.stringify(val));
      },
    },
    time: {
      type: DataTypes.DATE,
      get: function() {
        return moment(this.getDataValue('time'))
            .format('DD-MM-YYYY h:mm:ss');
      },
    },
    createdAt: {
      type: DataTypes.DATE,
      get: function() {
        return moment(this.getDataValue('createdAt'))
            .format('DD-MM-YYYY h:mm:ss');
      },
    },
    updatedAt: {
      type: DataTypes.DATE,
      get: function() {
        return moment(this.getDataValue('updatedAt'))
            .format('DD-MM-YYYY h:mm:ss');
      },
    },
    userid: DataTypes.INTEGER,
    taskname: DataTypes.STRING,
    latitude: DataTypes.STRING,
    longitude: DataTypes.STRING,
  }, {});
  todo.associate = function(models) {
    // associations can be defined here
  };
  return todo;
};
